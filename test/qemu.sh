#!/bin/bash -e

KERNEL_PATH="/boot/vmlinuz-4.19.20-1rodete1-amd64"

qemu-system-x86_64 \
		-vga none \
		-nographic \
		-m 256 \
		-display none \
		-device virtio-rng-pci \
		-device virtio-serial \
		-device virtio-scsi-pci,id=scsi \
		-kernel "$KERNEL_PATH" \
		-initrd initramfs.cpio \
		-no-reboot \
		-append "console=ttyS0,115200 panic=-1 acpi=off nosmp ip=dhcp"
