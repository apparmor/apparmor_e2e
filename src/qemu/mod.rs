use std::collections;
use std::collections::hash_map::Entry::{Occupied, Vacant};
use std::ffi;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::os::unix::fs::*;
use std::path;
use std::process;
use std::result;
use std::str;

extern crate libc;

/// QEMU process must print this string to stdout to indicate that the internal
/// tests failed.
pub static TESTS_FAILED: &str = ":: Tests failed";

mod cpio;

type Result<T> = result::Result<T, failure::Error>;

fn path_to_string<P: AsRef<path::Path>>(path: P) -> String {
    return path.as_ref().to_string_lossy().to_string();
}

fn canonical_path<P: AsRef<path::Path>>(path: P) -> Result<path::PathBuf> {
    let path = path.as_ref();
    return Ok(path
        .strip_prefix("/")
        .map_err(|e| format_err!("determining canonical path of {} {}", path.display(), e))?
        .to_path_buf());
}

/// A QEMU image builder and test runner.
pub struct Runner {
    to_copy: Vec<(String, String)>,
}

impl Runner {
    pub fn new() -> Runner {
        return Runner { to_copy: vec![] };
    }

    /// Add a file from the host to the initrd at a given path.
    pub fn add<P: AsRef<path::Path>>(&mut self, host_path: P, guest_path: P) -> &mut Runner {
        self.to_copy.push((
            host_path.as_ref().to_string_lossy().to_string(),
            guest_path.as_ref().to_string_lossy().to_string(),
        ));
        return self;
    }

    pub fn run<P: AsRef<path::Path>>(&mut self, kernel_path: P) -> Result<()> {
        let t = tempdir()?;
        let run_result = reexec(
            t.clone(),
            kernel_path.as_ref().to_string_lossy().to_string(),
            &self.to_copy,
        );
        let rm_result = fs::remove_dir_all(t).map_err(|e| format_err!("removing directory {}", e));
        if run_result.is_err() {
            return run_result;
        }
        return rm_result;
    }
}

fn tempdir() -> Result<String> {
    let tmpl = ffi::CString::new("/tmp/apparmor_XXXXXX")
        .map_err(|e| format_err!("allocating cstring {}", e))?
        .into_raw();

    let dir = unsafe { libc::mkdtemp(tmpl) };
    if dir.is_null() {
        return Err(format_err!("mkdtemp {}", io::Error::last_os_error()));
    }
    return unsafe { ffi::CString::from_raw(dir) }
        .into_string()
        .map_err(|e| format_err!("allocating cstring {}", e));
}

/// Is the process running in QEMU?
pub fn in_qemu() -> bool {
    return match fs::read_to_string("/proc/cpuinfo") {
        Ok(s) => s.contains("QEMU"),
        Err(_) => process::id() == 1,
    };
}

fn copy<R: io::Read, W1: io::Write, W2: io::Write>(
    r: &mut R,
    w1: &mut W1,
    w2: &mut W2,
) -> io::Result<()> {
    let mut buf = [0; 2048];
    loop {
        let size = r.read(&mut buf)?;
        if size == 0 {
            return Ok(());
        }
        let b = &buf[..size];
        w1.write(b)?;
        w2.write(b)?;
    }
}

/// Rerun current process in QEMU;
fn reexec<P: AsRef<path::Path>>(
    tempdir: P,
    kernel: P,
    files: &Vec<(String, String)>,
) -> Result<()> {
    let initrd_path = tempdir.as_ref().join("initrd");
    let mut f = fs::File::create(initrd_path.clone())
        .map_err(|e| format_err!("creating temp file {}, {}", initrd_path.display(), e))?;

    let mut initrd = Initrd::new(&mut f);
    for (from, to) in files {
        initrd.add_as(from.clone(), to.clone())?;
    }
    // libc-bin:
    initrd.add("/etc/ld.so.conf")?;
    initrd.write_trailer()?;

    let mut child = process::Command::new("qemu-system-x86_64")
        .args(&["-vga", "none"])
        .args(&["-nographic"])
        .args(&["-m", "256"])
        .args(&["-display", "none"])
        .args(&["-device", "virtio-rng-pci"])
        .args(&["-device", "virtio-serial"])
        .args(&["-device", "virtio-scsi-pci,id=scsi"])
        .args(&["-kernel", kernel.as_ref().to_str().unwrap()])
        .args(&["-initrd", initrd_path.to_str().unwrap()])
        .args(&["-no-reboot"])
        .args(&[
            "-append",
            "console=ttyS0,115200 apparmor=1 security=apparmor panic=-1 acpi=off nosmp ip=dhcp",
        ])
        .stdout(process::Stdio::piped())
        .spawn()
        .map_err(|e| format_err!("spawning QEMU process {}", e))?;

    let mut stdout: Vec<u8> = vec![];
    copy(
        &mut child
            .stdout
            .take()
            .ok_or(format_err!("child stdout not piped"))?,
        &mut stdout,
        &mut io::stderr(),
    )
    .map_err(|e| format_err!("capturing child stdout {}", e))?;

    let rc = child
        .wait()
        .map_err(|e| format_err!("waiting for QEMU to exit {}", e))?;

    if rc.success() {
        if str::from_utf8(&stdout)
            .map_err(|e| format_err!("parse QEMU stdout {}", e))?
            .contains(TESTS_FAILED)
        {
            return Err(format_err!("tests failed"));
        }
        return Ok(());
    }
    return Err(format_err!(
        "qemu failed with exit code {}",
        rc.code().map_or(-1, |c| c)
    ));
}

/// An initrd builder
struct Initrd<W: io::Write> {
    b: cpio::Builder<W>,
    // Directories and files already tracked in the initrd.
    //
    // For files, this is only used to determine presence in the initrd, to
    // avoid adding the same shared library multiple times.
    //
    // For directories, the map is used to determine presence, as well as map
    // symlinked directories to their real path. For example, if `/sbin` is
    // a symlink to `/usr/sbin`, this map will indicate that `/sbin/foo` should
    // be written as `/usr/sbin/foo`.
    seen: collections::HashMap<String, String>,
}

impl<W: io::Write> Initrd<W> {
    fn new(w: W) -> Initrd<W> {
        return Initrd {
            b: cpio::Builder::new(w),
            seen: collections::HashMap::new(),
        };
    }

    /// Add a file, its parent directories, and all shared libraries to the
    /// initrd.
    fn add<P: AsRef<path::Path>>(&mut self, path: P) -> Result<()> {
        return self.add_as(path.as_ref(), path.as_ref());
    }

    /// Add a file to the initrd renamed under a different path. Shared
    /// libraries will be added as their original path. The parent directory of
    /// the target path must exist.
    fn add_as<P: AsRef<path::Path>>(&mut self, path: P, as_path: P) -> Result<()> {
        let path = path.as_ref();
        let as_path = as_path.as_ref();

        // For files, self.seen is only used to track if the file's been added
        // to the initrd already. The value isn't important.
        let as_path_str = path_to_string(as_path);
        match self.seen.entry(as_path_str.clone()) {
            Occupied(_) => return Ok(()),
            Vacant(entry) => entry.insert(as_path_str.clone()),
        };

        if !path.is_absolute() {
            return Err(format_err!(
                "path added to initrd image is not absolute {}",
                path.display()
            ));
        }
        if !as_path.is_absolute() {
            return Err(format_err!(
                "target initrd path is not absolute {}",
                path.display()
            ));
        }

        let parent = match as_path.parent() {
            None => return Ok(()),
            Some(p) => p,
        };

        // Add the parent directory
        let new_parent = self.add_dir(parent)?;
        let target = new_parent.join(as_path.file_name().ok_or(format_err!(
            "failed to determine basename of {}",
            as_path.display()
        ))?);

        let stat = fs::symlink_metadata(path)
            .map_err(|e| format_err!("stat {} {}", path_to_string(path), e))?;
        if stat.is_dir() {
            return Err(format_err!(
                "expected file found directory at path {}",
                path.display(),
            ));
        }

        let mut h = cpio::Header::from_path(path)
            .map_err(|e| format_err!("building cpio header for {} {}", path_to_string(path), e))?;
        h.name = path_to_string(canonical_path(target)?);
        self.write_header(h)?;

        if stat.is_file() {
            let mut f = fs::File::open(path)
                .map_err(|e| format_err!("opening file {} {}", path.display(), e))?;
            io::copy(&mut f, &mut self.b)
                .map_err(|e| format_err!("copying file {} {}", path.display(), e))?;
            for deps in &ldd(path)? {
                self.add(deps.to_string())?;
            }
            return Ok(());
        }

        let link =
            fs::read_link(path).map_err(|e| format_err!("read link {} {}", path.display(), e))?;
        self.b
            .write(link.to_string_lossy().as_bytes())
            .map_err(|e| format_err!("writing to initrd archive {}", e))?;
        if link.is_absolute() {
            return self.add(link);
        }
        return self.add(new_parent.join(link));
    }

    /// Add a directory and all parent directories to the initrd.
    ///
    /// This returns the canonical path of the directory. For example, if the
    /// path is '/lib/x86_64-linux-gnu' and '/lib' is a symlink to '/usr/lib'
    /// this method returns `Ok("/usr/lib/x86_64-linux-gnu")`.
    fn add_dir<P: AsRef<path::Path>>(&mut self, path: P) -> Result<path::PathBuf> {
        let path = path.as_ref();

        let parent = match path.parent() {
            None => return Ok(path.to_path_buf()),
            Some(p) => p,
        };

        // self.seen holds the real path to write a directory to, evaluating
        // parent symlinks. If we've already evaluated this directory, just
        // return its real path.
        let path_str = path_to_string(path);
        match self.seen.get(&path_str) {
            Some(path_to) => return Ok(path::PathBuf::from(path_to)),
            None => {}
        }

        // Resolve any symlinks in the parent directories and determine this
        // directory's real path.
        let new_parent = self.add_dir(parent)?;
        let target = new_parent.join(path.file_name().ok_or(format_err!(
            "failed to determine basename of {}",
            path.display()
        ))?);
        self.seen.insert(path_str, path_to_string(target.clone()));

        let stat = fs::symlink_metadata(target.clone())
            .map_err(|e| format_err!("stat {} {}", path.display(), e))?;

        if stat.is_file() {
            return Err(format_err!(
                "expected directory found file at path {}",
                path.display(),
            ));
        }

        if stat.is_dir() {
            self.write_header(cpio::Header::directory(
                canonical_path(target.clone())?,
                stat.permissions().mode(),
            ))?;
            return Ok(target.clone());
        }

        let mut link = fs::read_link(target.clone())
            .map_err(|e| format_err!("read link {} {}", target.display(), e))?;
        self.write_header(cpio::Header::symlink(target.clone(), link.clone()))?;
        self.b
            .write(link.clone().to_string_lossy().as_bytes())
            .map_err(|e| format_err!("writing to initrd archive {}", e))?;

        // Add whatever the path points to.
        if !link.is_absolute() {
            link = parent.join(link);
        }
        return self.add_dir(link);
    }

    fn write_trailer(&mut self) -> Result<()> {
        self.write_header(cpio::Header::directory("proc", 0o755))?;
        self.write_header(cpio::Header::directory("sys", 0o755))?;
        self.write_header(cpio::Header::directory("dev", 0o755))?;
        self.write_header(cpio::Header::directory("mnt", 0o755))?;
        return self
            .b
            .write_trailer()
            .map_err(|e| format_err!("write initrd trailer {}", e));
    }

    fn write_header(&mut self, h: cpio::Header) -> Result<()> {
        return self
            .b
            .write_header(&h)
            .map_err(|e| format_err!("write initrd header {}", e));
    }
}

fn ldd<P: AsRef<path::Path>>(p: P) -> Result<Vec<String>> {
    let out = process::Command::new("ldd")
        .arg(p.as_ref())
        .output()
        .map_err(|e| format_err!("running ldd {}", e))?;
    if !out.status.success() {
        return Ok([].to_vec());
    }
    let output =
        String::from_utf8(out.stdout).map_err(|e| format_err!("parsing ldd output {}", (e)))?;
    return Ok(output
        .split(char::is_whitespace)
        .filter(|s| s.starts_with('/'))
        .map(|s| s.to_string())
        .collect());
}
