//! Minimal flag parsing.
//!
//! This module provides command line parsing capabilities. It aims to be a
//! lighter alternative to existing crates.

use std::collections;
use std::fmt;
use std::result;

type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    NoArg(String),
    NoArgProvided(String),
    UnknownFlag(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Error::*;
        match self {
            NoArg(flag) => write!(f, "flag takes no arguments {}", flag),
            NoArgProvided(flag) => write!(f, "no argument provided for flag {}", flag),
            UnknownFlag(flag) => write!(f, "unrecognized flag {}", flag),
        }
    }
}

/// A user defined command line flag.
#[derive(Debug, Clone)]
pub struct Flag {
    name_long: String,
    name_short: String,
    value: bool,
}

impl Flag {
    /// Builds a new flag. Flags must use long() or short() match a command
    /// line argument. Flags may specify both a long and short value.
    pub fn new() -> Flag {
        return Flag {
            name_long: "".to_string(),
            name_short: "".to_string(),
            value: false,
        };
    }

    /// Match a long flag, e.g. "--help".
    pub fn long(&mut self, long: &str) -> &mut Flag {
        self.name_long = long.to_string();
        return self;
    }

    /// Match a short flag value, e.g. "-h".
    pub fn short(&mut self, short: &str) -> &mut Flag {
        self.name_short = short.to_string();
        return self;
    }

    /// Indicate that this flag takes a value. The value may provided in the
    /// same argument using an "=" character. For example "--foo=bar".
    pub fn takes_value(&mut self) -> &mut Flag {
        self.value = true;
        return self;
    }
}

fn parse_arg_value(flag: String) -> (String, Option<String>) {
    if !flag.contains("=") {
        return (flag, None);
    }
    let s: Vec<&str> = flag.splitn(2, "=").collect();
    return (s[0].to_string(), Some(s[1].to_string()));
}

pub struct FlagSet {
    flags: Vec<Flag>,
}

impl FlagSet {
    /// Build a new flag set.
    pub fn new() -> FlagSet {
        return FlagSet { flags: vec![] };
    }

    /// Add a flag to the flag set.
    pub fn flag(&mut self, flag: &Flag) -> &mut FlagSet {
        self.flags.push(flag.clone());
        return self;
    }

    /// Parse a command line.
    pub fn parse(&mut self, args: Vec<String>) -> Result<Parsed> {
        let mut p = Parsed {
            values: collections::HashMap::new(),
            remaining_args: vec![],
        };

        let mut it = args.iter().peekable();
        let mut skip_next = false;
        loop {
            let val = match it.next() {
                Some(val) => val,
                None => return Ok(p),
            };
            if skip_next {
                skip_next = false;
                continue;
            }

            // TODO: support "--" value

            let mut found_flag = false;
            for prefix in vec!["--", "-"] {
                if !val.starts_with(prefix) {
                    continue;
                }

                let (key, keyval) = parse_arg_value(val.replacen(prefix, "", 1));
                for flag in self.flags.clone() {
                    if prefix == "--" && flag.name_long != key.clone()
                        || prefix == "-" && flag.name_short != key.clone()
                    {
                        continue;
                    }
                    found_flag = true;

                    if !flag.value {
                        if keyval.is_some() {
                            return Err(Error::NoArg(key.clone()));
                        }
                        if flag.name_long != "" {
                            p.values.insert(flag.name_long, "".to_string());
                        }
                        if flag.name_short != "" {
                            p.values.insert(flag.name_short, "".to_string());
                        }
                        break;
                    }

                    match keyval.clone() {
                        Some(v) => {
                            p.values.insert(key.clone(), v);
                        }
                        None => match it.peek() {
                            None => return Err(Error::NoArgProvided(key.clone())),
                            Some(val) => {
                                if val.starts_with("-") {
                                    return Err(Error::NoArgProvided(key.clone()));
                                }
                                skip_next = true;
                                p.values.insert(key.clone(), val.to_string());
                            }
                        },
                    };
                }
                if found_flag {
                    break;
                }
                return Err(Error::UnknownFlag(key.clone()));
            }
            if !found_flag {
                p.remaining_args.push(val.clone());
            }
        }
    }
}

pub struct Parsed {
    values: collections::HashMap<String, String>,
    remaining_args: Vec<String>,
}

impl Parsed {
    /// Indicates if a flag was present in the command line. Short or long
    /// values may be provided.
    pub fn present(&self, flag: &str) -> bool {
        return self.values.contains_key(&flag.to_string());
    }

    /// If the given flag was present and takes a value, returns the value of
    /// the flag.
    ///
    /// If the flags wasn't present, the value will be None.
    pub fn value(&self, flag: &str) -> Option<String> {
        return self.values.get(&flag.to_string()).map(|s| (*s).to_string());
    }

    /// Any non-flag arguments provided.
    pub fn args(&self) -> Vec<String> {
        return self.remaining_args.clone();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_flag() {
        let f = FlagSet::new()
            .flag(Flag::new().short("h").long("help"))
            .parse(vec!["-h".to_string()])
            .unwrap();
        assert!(f.present("h"));
        assert!(f.present("help"));
    }

    #[test]
    fn parse_flag_with_values() {
        let f = FlagSet::new()
            .flag(Flag::new().short("h").long("help"))
            .flag(Flag::new().short("v").long("verbose"))
            .flag(Flag::new().long("foo").takes_value())
            .flag(Flag::new().long("spam").takes_value())
            .parse(
                vec![
                    "-h",
                    "--verbose",
                    "arg1",
                    "--foo",
                    "bar",
                    "--spam=eggs",
                    "arg2",
                ]
                .iter()
                .map(|s| s.to_string())
                .collect(),
            )
            .unwrap();
        assert!(f.present("h"));
        assert!(f.present("help"));
        assert!(f.present("v"));
        assert!(f.present("verbose"));
        assert!(f.present("foo"));
        assert_eq!(f.value("foo"), Some("bar".to_string()));
        assert_eq!(f.value("spam"), Some("eggs".to_string()));
        assert_eq!(f.args(), vec!["arg1", "arg2"]);
    }

    #[test]
    fn parse_flag_no_value_expected() {
        let r = FlagSet::new()
            .flag(Flag::new().short("h").long("help"))
            .parse(vec!["--help=bar"].iter().map(|s| s.to_string()).collect());
        assert!(r.is_err());
    }

    #[test]
    fn parse_flag_with_value_followed_by_flag() {
        let r = FlagSet::new()
            .flag(Flag::new().long("foo").takes_value())
            .flag(Flag::new().long("bar"))
            .parse(
                vec!["--foo", "--bar"]
                    .iter()
                    .map(|s| s.to_string())
                    .collect(),
            );
        assert!(r.is_err());
    }

    #[test]
    fn parse_flag_no_value_given() {
        let r = FlagSet::new()
            .flag(Flag::new().long("bar").takes_value())
            .parse(vec!["--bar"].iter().map(|s| s.to_string()).collect());
        assert!(r.is_err());
    }
}
